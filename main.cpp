#include <iostream>

using namespace std;

void i ()
{
 cout <<  "_____ "  << endl;
 cout <<"    |  "<< endl;
 cout << "   | "<< endl;
 cout <<"    |  "<< endl;
 cout <<"    |  "<< endl;
 cout <<"    |  "<< endl;
 cout << " _____"<< endl;
}
void v()
{
cout <<" \\             //"<< endl;
cout <<"  \\           //"<< endl;
cout <<"   \\         //"<< endl;
cout <<"    \\       //"<< endl;
cout <<"     \\     //"<< endl;
cout <<"      \\   //"<< endl;
cout <<"       \\ // "<< endl;
cout <<"        \\/ "<< endl;
}
void a()
{
  cout <<"    //\\      "<< endl;
 cout <<"    //  \\    "<< endl;
 cout <<"   //    \\    "<< endl;
 cout <<"  //      \\   "<< endl;
 cout <<" //--------\\  "<< endl;
 cout <<"//          \\ "<< endl;
}
void n()
{
 cout <<"     //\\          //"<< endl;
 cout <<"    //  \\        //"<< endl;
 cout <<"   //    \\      //"<< endl;
 cout <<"  //      \\    //"<< endl;
 cout <<" //        \\  //"<< endl;
 cout <<"//          \\// "<< endl;
}
int main()
{   
    i();
    v();
    a();
    n();
    cout << "Hello world!" << endl;
    return 0;
}
